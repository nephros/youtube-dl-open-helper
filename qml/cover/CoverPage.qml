import QtQuick 2.0
import Sailfish.Silica 1.0

CoverBackground {
    id: cover

    property bool downloading: conf.isActive

    Label {
        id: coverLabel
        x: Theme.paddingLarge
        color: Theme.highlightColor
        anchors {
            bottom: logo.top
            topMargin: Theme.paddingLarge
            bottomMargin: Theme.paddingLarge * 3
            horizontalCenter: parent.horizontalCenter
        }
        //: name displayed on cover
        //% "Video\nDownloader"
        text: qsTrId("youtube-dl-clip-cover")

        horizontalAlignment: Label.AlignHCenter
        truncationMode: TruncationMode.Elide
        wrapMode: Text.WordWrap
        font.pixelSize: Theme.fontSizeSmall
    }
    Image {
        id: logo
        anchors.centerIn: cover
        source: "image://theme/icon-m-cloud-download?" + ( downloading ? Theme.highlightColor :  Theme.secondaryColor )
    }
    Image {
        anchors {
            top: logo.bottom
            horizontalCenter: parent.horizontalCenter
        }
        source: "image://theme/icon-m-call-recording-on-light?" + ( downloading ? Theme.highlightColor :  Theme.secondaryHighlightColor )
        SequentialAnimation on opacity {
            running: ( cover.downloading && cover.status == Cover.Active )
            loops: Animation.Infinite
            alwaysRunToEnd: true
            NumberAnimation { from: 1.0; to: 0.25; duration: 1500; easing.type: Easing.InOutQuad }
            NumberAnimation { from: 0.25; to: 1.0; duration: 1500; easing.type: Easing.InOutQuad }
            PauseAnimation { duration: 750 }
        }
    }

    TextField {
        id: coverField
        visible: false
    }
    CoverActionList {
        id: actionPaste
        enabled: Clipboard.hasText
        iconBackground: false
        CoverAction {
            iconSource: "image://theme/icon-s-clipboard" + ( parent.enabled ? Theme.highlightColor : Theme.secondaryHighlightColor )
            onTriggered: {
                //TODO: this doesn't work for some reason...
                //if (Clipboard.hasText) {
                    coverField.paste()
                    coverField.text = coverField.text.replace("https", "ytdls")
                    coverField.text = coverField.text.replace("http", "ytdl")
                    console.debug("Cover Action:" + coverField.text + " (" + Clipboard.text + ")" )
                    Qt.openUrlExternally(coverField.text)
                //} else {
                //    console.warn("Cover Action: clipboard has no text")
                //}
            }
        }
    } //CoverActionList
} //Cover

/*
keep this here so qtcreator leaves the below intact:
*/

// vim: expandtab ts=4 mouse-=a filetype=javascript

