import QtQuick 2.0
import Sailfish.Silica 1.0
import Nemo.Configuration 1.0
import Nemo.DBus 2.0

Page {
    id: page
    allowedOrientations: Orientation.All

    // this is just so we can have the categories from the json file translated
    QtObject {
        //: the name of the settings category
        //% "MIME Handlers"
        property string jsonText1: qsTrId("settings-system-mime-category")
        //: the name of the settings menu entry
        //% "Video Downloader"
        property string jsonText2: qsTrId("settings-youtube-dl-title")
    }

    ConfigurationGroup {
        id: conf
        path: "/org/nephros/youtube-dl/mime-helper"
        property bool enabled: true
    }

    DBusInterface {
        id: systemd

        bus: DBus.SessionBus
        service: "org.freedesktop.systemd1"
        path: "/org/freedesktop/systemd1"
        iface: "org.freedesktop.systemd1.Manager"
        signalsEnabled: true

        function runService() {
            systemd.typedCall("StartUnit", [{
                                                "type": "s",
                                                "value": "youtube-dl-open-installer.service"
                                            }, {
                                                "type": "s",
                                                "value": "fail"
                                            }], function (result) {
                                                console.debug('Systemd call result: ' + result)
                                            }, function (error, message) {
                                                console.warn('Systemd call failed: ',
                                                            error, 'message: ',
                                                            message)
                                            })
        }
    }

    SilicaFlickable {
        id: flick
        anchors.fill: parent
        contentHeight: column.height

        Column {
            id: column
            width: parent.width

            PageHeader {
                //: settings page title/header
                //% "Video URL download helper"
                title: qsTrId("settings-youtube-dl-header")
            }

            TextSwitch {
                id: videoSwitch
                //: settings page switch name
                //% "Enable downloading video URLs"
                text: qsTrId("settings-youtube-dl-switch")
                //: settings menu switch description
                //% "Description of the feature here"
                description: qsTrId("settings-youtube-dl-description")

                checked: conf.enabled
                onClicked: {
                    systemd.runService()
                }
            }
        } //Column
        VerticalScrollDecorator {
            flickable: column
        }
    } //Flickable
} //Page

//vim: expandtab ts=4 filetype=javascript

