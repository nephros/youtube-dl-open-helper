<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name></name>
    <message id="settings-system-mime-category">
        <source>MIME Handlers</source>
        <extracomment>the name of the settings category</extracomment>
        <translation>MIME Funktionen</translation>
    </message>
    <message id="settings-youtube-dl-title">
        <source>Video Downloader</source>
        <extracomment>the name of the settings menu entry</extracomment>
        <translation>Video Downloader</translation>
    </message>
    <message id="settings-youtube-dl-header">
        <source>Video URL download helper</source>
        <extracomment>settings page title/header</extracomment>
        <translation>Video-URL Herunterladehelferlein</translation>
    </message>
    <message id="settings-youtube-dl-switch">
        <source>Enable downloading video URLs</source>
        <extracomment>settings page switch name</extracomment>
        <translation>Herunterladen aktivieren</translation>
    </message>
    <message id="settings-youtube-dl-description">
        <source>Description of the feature here</source>
        <extracomment>settings menu switch description</extracomment>
        <translation>Wenn dies aktiv ist, bieten der Browser und andere Apps an, Videolinks herunterzuladen.

Es wird nur für URLs klappen, die von youtube-dl unterstützt werden.</translation>
    </message>
    <message id="youtube-dl-clip-header">
        <source>Video URL download helper</source>
        <extracomment>clipboard page title/header</extracomment>
        <translation type="obsolete">Video-URL Herunterladehelferlein</translation>
    </message>
</context>
</TS>
