<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en">
<context>
    <name></name>
    <message id="settings-system-mime-category">
        <source>MIME Handlers</source>
        <oldsource>MIME Handlers</oldsource>
        <extracomment>the name of the settings category</extracomment>
        <translation>MIME Handlers</translation>
    </message>
    <message id="settings-youtube-dl-title">
        <source>Video Downloader</source>
        <oldsource>Video Downloader</oldsource>
        <extracomment>the name of the settings menu entry</extracomment>
        <translation>Video Downloader</translation>
    </message>
    <message id="settings-youtube-dl-header">
        <source>Video URL download helper</source>
        <oldsource>Video URL download helper</oldsource>
        <extracomment>settings page title/header</extracomment>
        <translation>Video URL download helper</translation>
    </message>
    <message id="settings-youtube-dl-switch">
        <source>Enable downloading video URLs</source>
        <oldsource>Enable downloading video URLs</oldsource>
        <extracomment>settings page switch name</extracomment>
        <translation>Enable downloading video URLs</translation>
    </message>
    <message id="settings-youtube-dl-description">
        <source>Description of the feature here</source>
        <extracomment>settings menu switch description</extracomment>
        <translation>Enabling this will cause the Browser and other apps to allow you to select a Video Download option for URL links.
Note that only URLs supported by youtube-dl will work.</translation>
    </message>
</context>
</TS>
