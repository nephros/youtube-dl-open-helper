<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name></name>
    <message id="settings-youtube-dl-header">
        <source>Video URL download helper</source>
        <extracomment>settings page title/header</extracomment>
        <translation>视频链接下载助手</translation>
    </message>
    <message id="settings-youtube-dl-switch">
        <source>Enable downloading video URLs</source>
        <extracomment>settings page switch name</extracomment>
        <translation>开启下载视频链接</translation>
    </message>
    <message id="settings-youtube-dl-description">
        <source>Description of the feature here</source>
        <extracomment>settings menu switch description</extracomment>
        <translation>启用此功能将使浏览器和其它应用程序允许你为 URL 链接选择视频下载选项。

视频将被储存在 ~/Videos/youtube-dl 目录中，并显示于媒体应用程序内。

同时，我们会在之后尝试启动下载的文件来播放视频。如果远程方做了一些狡猾和邪恶的事情，可能会存在安全风险。
请负责任地下载。

注意，只有 youtube-dl 支持的 URL 才可以使用此项功能。</translation>
    </message>
    <message id="settings-system-mime-category">
        <source>MIME Handlers</source>
        <extracomment>the name of the settings category</extracomment>
        <translation>MIME 处理器</translation>
    </message>
    <message id="settings-youtube-dl-title">
        <source>Video Downloader</source>
        <extracomment>the name of the settings menu entry</extracomment>
        <translation>视频下载器</translation>
    </message>
</context>
</TS>
