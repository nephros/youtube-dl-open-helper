import QtQuick 2.0
import Sailfish.Silica 1.0
import Nemo.Configuration 1.0

ApplicationWindow { id: app

    allowedOrientations: Orientation.All

    ConfigurationGroup { id: conf
        path: "/org/nephros/youtube-dl/mime-helper"
        synchronous: true
        property bool audioOnly
        property bool isActive
        property string storagePath
    }
    initialPage: Qt.resolvedUrl("pages/MainPage.qml")
    cover: Qt.resolvedUrl("cover/CoverPage.qml")
}

// vim: ft=javascript expandtab ts=4 sw=4 st=4
