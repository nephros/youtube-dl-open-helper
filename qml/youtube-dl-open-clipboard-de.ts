<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name></name>
    <message id="settings-youtube-dl-open">
        <source>Open file after download</source>
        <extracomment>settings page open file</extracomment>
        <translation>Nach dem Herunterladen abspielen</translation>
    </message>
    <message id="settings-youtube-dl-open-description">
        <source>This is a security risk</source>
        <oldsource>This will try to open the downloaded file after downloading</oldsource>
        <extracomment>settings page open file description</extracomment>
        <translation>Das ist ein Sicherheitsrisiko, falls die Gegenseite Böses im Sinn hat.
Lade verantworungsvoll herunter!</translation>
    </message>
    <message id="settings-youtube-dl-path-desc">
        <source>Path to store videos under</source>
        <extracomment>settings menu switch description</extracomment>
        <translation type="vanished">Video-Ablageverzeichnis</translation>
    </message>
    <message id="settings-youtube-dl-path-label">
        <source>A directory under: </source>
        <extracomment>settings menu switch label</extracomment>
        <translation>Ein Verzeichnis unter: </translation>
    </message>
    <message id="settings-youtube-dl-path-enable">
        <source>Specify storage location</source>
        <oldsource>Specify download location</oldsource>
        <extracomment>settings enable path input</extracomment>
        <translation>Ablageverzeichis anpassen</translation>
    </message>
    <message id="settings-youtube-dl-path-enable-desc">
        <source>This should be a simple name without special characters to avoid breakage. It will be created under </source>
        <oldsource>This should be a simple name without special characters to avoid breakage</oldsource>
        <extracomment>settings enable path description</extracomment>
        <translation>Gib einen einfachen Verzeichnisnamen ohne spezielle Zeichen an. Es wird gespeichert unter</translation>
    </message>
    <message id="video-quality">
        <source>Video Quality</source>
        <translation>Bevorzugte Videoqualität</translation>
    </message>
    <message id="vq-automatic">
        <source>automatic \(best\)</source>
        <oldsource>automatic (best)</oldsource>
        <translation>Automatisch (beste)</translation>
    </message>
    <message id="audio-quality-vbr">
        <source>Audio Quality (VBR)</source>
        <oldsource>Audio Qualit (VBR)</oldsource>
        <translation type="vanished">Audioqualität (VBR)</translation>
    </message>
    <message id="prefer-free-formats">
        <source>Prefer Free formats</source>
        <translation>Freie/Offene Formate bevorzugen</translation>
    </message>
    <message id="options">
        <source>Options</source>
        <translation>Optionen</translation>
    </message>
    <message id="vq-1080">
        <source>max 1080p</source>
        <translation>Bis zu 1080p</translation>
    </message>
    <message id="vq-720">
        <source>max 720p</source>
        <translation>Bis zu 720p</translation>
    </message>
    <message id="vq-480">
        <source>max 480p</source>
        <translation>Bis zu 480p</translation>
    </message>
    <message id="vq-worst">
        <source>worst</source>
        <translation>Schlechteste (wieso nicht)</translation>
    </message>
    <message id="vq-all">
        <source>all available (use with caution!)</source>
        <translation>Alle verfügbaren (Vorsicht!)</translation>
    </message>
    <message id="settings-youtube-dl-prefer-use-netrc">
        <source>Respect ~/.netrc if it exists</source>
        <translation>Verwende ~/.netrc falls vorhanden</translation>
    </message>
    <message id="settings-youtube-dl-prefer-use-netrc-description">
        <source>You can create a file called ~/.netrc and store user and password information there. This can be used to enable services which require a login to access. See the About page for more information.</source>
        <oldsource>You can create a file called ~/.netrc and store user and password information there. This can be used to enable services which require a login to access. See the following URL for more information: </oldsource>
        <extracomment>netrc description</extracomment>
        <translation>Die Datei ~/.netrc kann verwendet werden, im Nutzer- und Passwortinformation zu speichern. Dies wird dann für Seiten verwendet, die ein Einloggen erfordern. Für mehr siehe die Seite &quot;About&quot; </translation>
    </message>
    <message id="settings-youtube-dl-reset-remorse">
        <source>Clearing config settings</source>
        <oldsource>Clearing all config values</oldsource>
        <extracomment>reset remorse popup text</extracomment>
        <translation>Setze Einstellungen zurück</translation>
    </message>
    <message id="settings-youtube-dl-reset">
        <source>Reset all to Default</source>
        <extracomment>reset button</extracomment>
        <translation>Einstellungen zurücksetzen</translation>
    </message>
    <message id="youtube-dl-clip-paste-label">
        <source>A video URL</source>
        <extracomment>paste area</extracomment>
        <translation>Eine Video-URL</translation>
    </message>
    <message id="youtube-dl-clip-header">
        <source>Video URL download helper</source>
        <extracomment>clipboard page title/header</extracomment>
        <translation>Video-URL Herunterladehelferlein</translation>
    </message>
    <message id="youtube-dl-clip-cover">
        <source>Video\nDownloader</source>
        <extracomment>name displayed on cover</extracomment>
        <translation>Video Downloader</translation>
    </message>
    <message id="youtube-dl-clip-explain">
        <source>If what you have in your clipboard is a video URL, paste it here. You can either download right away, or modify the clipboard contents first, and then download.</source>
        <extracomment>short explanation below buttons</extracomment>
        <translation>Wenn deine Zwischenablage eine Video-URL enthält, füge sie hier ein. Du kannst das Herunterladen entweder sofort starten, oder den Inhalt erst editieren, und dann starten.</translation>
    </message>
    <message id="youtube-dl-clip-go-button">
        <source>Go</source>
        <extracomment>go button</extracomment>
        <translation>Los!</translation>
    </message>
    <message id="youtube-dl-clip-paste-button">
        <source>Paste</source>
        <extracomment>paste button</extracomment>
        <translation>Einfügen</translation>
    </message>
    <message id="youtube-dl-clip-about">
        <source>This is a convoluted collection of glue to trigger the excellent youtube-dl program. See below for links.</source>
        <oldsource>This is part of a collection of small utilities distributed on Openrepos. It mainly works because of a convoluted layer of glue to the excellent youtube-dl program. See below for links.</oldsource>
        <extracomment>about text</extracomment>
        <translation>Dies ist eine Sammlung von unterschiedlichsten Verbindungstechniken zum wunderbaren Programm Youtube-DL. Weblinks siehe unten.</translation>
    </message>
    <message id="youtube-dl-clip-aboutlink">
        <source>About</source>
        <extracomment>about button</extracomment>
        <translation>Über</translation>
    </message>
    <message id="youtube-dl-clip-settingslink">
        <source>Settings</source>
        <extracomment>settings button</extracomment>
        <translation>Einstellungen</translation>
    </message>
    <message id="youtube-dl-clip-about-thanks">
        <source>Credits/Thanks/Links</source>
        <translation>Credits/Danksagungen/Weblinks</translation>
    </message>
    <message id="youtube-dl-about-translations">
        <source>Translations</source>
        <translation>Übersetzungen</translation>
    </message>
    <message id="youtube-dl-audio-only-enable">
        <source>Only keep audio</source>
        <extracomment>settings enable audio extraction</extracomment>
        <translation>Nur Ton</translation>
    </message>
    <message id="youtube-dl-audio-only-enable-desc">
        <source>Download and extract only the audio track. It will be stored under</source>
        <oldsource>Download and extract only the audio track. It will be stored under ~/Music</oldsource>
        <extracomment>settings enable audio description</extracomment>
        <translation>Video herunterladen, Ton extrahieren und speichern. Die Datei entsteht unter </translation>
    </message>
    <message id="youtube-dl-audio-quality">
        <source>Audio Quality</source>
        <extracomment>audio quality slider</extracomment>
        <translation>Audioqualität</translation>
    </message>
    <message id="settings-youtube-dl-header">
        <source>Video URL download helper</source>
        <extracomment>settings page title/header</extracomment>
        <translation type="obsolete">Video-URL Herunterladehelferlein</translation>
    </message>
    <message id="settings-youtube-dl-sponsorblock-enable">
        <source>Remove annoying segments from Videos.</source>
        <extracomment>settings enable SponsorBlock</extracomment>
        <translation>Lästige Inhalte aus Videos entfernen</translation>
    </message>
    <message id="settings-youtube-dl-sponsorblock-enable-desc">
        <source>Use SponsorBlock integration to remove ads etc.</source>
        <extracomment>settings enable SponsorBlock description</extracomment>
        <translation>Verwendet SponsorBlock zum Werbung usw. zu entfernen</translation>
    </message>
</context>
</TS>
