import QtQuick 2.0
import Sailfish.Silica 1.0

Page { id: page
    allowedOrientations: Orientation.All

    readonly property string settingsPage: Qt.resolvedUrl("SettingsPage.qml")
    readonly property string aboutPage: Qt.resolvedUrl("AboutPage.qml")

    SilicaFlickable { id: flick
        anchors.fill: parent
        contentHeight: column.height

        PullDownMenu {
            MenuItem {
                //: about button
                //% "About"
                text: qsTrId("youtube-dl-clip-aboutlink")
                onClicked: pageStack.push(aboutPage)
            }
            MenuItem {
                //: settings button
                //% "Settings"
                text: qsTrId("youtube-dl-clip-settingslink")
                onClicked: pageStack.push(settingsPage)
            }
        }

        Column { id: column
            width: parent.width

            PageHeader { id: ph
                //: clipboard page title/header
                //% "Video URL download helper"
                title: qsTrId("youtube-dl-clip-header")
                anchors.horizontalCenter: parent.horizontalCenter
                Image {
                    x: Theme.horizontalPageMargin
                    anchors.verticalCenter: ph.verticalCenter
                    height: parent.height/2
                    width: height
                    fillMode: Image.PreserveAspectFit
                    source: conf.isActive ? "image://theme/icon-m-call-recording-on-light" : "image://theme/icon-m-call-recording-off?Theme.secondaryColor"
                    SequentialAnimation on opacity {
                        running: ( conf.isActive && page.status == PageStatus.Active )
                        loops: Animation.Infinite
                        alwaysRunToEnd: true
                        NumberAnimation { from: 0.25; to: 1.0; duration: 1500; easing.type: Easing.InOutQuad }
                        NumberAnimation { from: 1.0; to: 0.25; duration: 1500; easing.type: Easing.InOutQuad }
                        PauseAnimation { duration: 750 }
                    }
                }
            }
            Label { id: explanation
                padding: Theme.paddingLarge
                width: parent.width - Theme.paddingLarge
                anchors.horizontalCenter: parent.horizontalCenter
                //: short explanation below buttons
                //% "If what you have in your clipboard is a video URL, paste it here. You can either download right away, or modify the clipboard contents first, and then download."
                text: qsTrId("youtube-dl-clip-explain")
                color: Theme.highlightColor
                wrapMode: Text.WordWrap
                truncationMode: TruncationMode.Elide
                font.pixelSize: Theme.fontSizeSmall
            }
            TextField { id: clipField
                width: parent.width - Theme.paddingLarge
                enabled: true
                softwareInputPanelEnabled: false
                focus: true
                inputMethodHints: Qt.ImhNoPredictiveText | Qt.ImhNoAutoUppercase
                horizontalAlignment: TextInput.AlignHCenter
                //: paste area
                //% "A video URL"
                label: qsTrId("youtube-dl-clip-paste-label")
                // TODO: do a little validation on names, we want this to be a simple word, no path, no variables etc.
                //validator: UrlValidator { }
                Component.onCompleted: {
                    if ( Clipboard.hasText && /[^htps:]+/.test(Clipboard.text) ) {
                        forceActiveFocus()
                        paste()
                    }
                }
                onClicked: {
                    softwareInputPanelEnabled: true
                }
                EnterKey.enabled: text.length > 0
                EnterKey.iconSource: "image://theme/icon-m-enter"
                EnterKey.onClicked: {
                    focus = false
                }
            }
            ButtonLayout { id: buttonRow
                Button { id: pngButton
                    text: qsTrId("youtube-dl-clip-paste-button") + "+" + qsTrId(
                              "youtube-dl-clip-go-button")
                    enabled: clipField.text.length === 0
                    onClicked: {
                        //console.debug("paste+go clicked")
                        clipField.forceActiveFocus()
                        clipField.selectAll()
                        clipField.paste()
                        clipField.text = clipField.text.replace("https",
                                                                "ytdls")
                        clipField.text = clipField.text.replace("http", "ytdl")
                        console.info("opening URL: " + clipField.text)
                        conf.sync()
                        Qt.openUrlExternally(clipField.text)
                        clipField.text = ""
                        app.deactivate()
                    }
                }
                Button { id: pogButton
                    text: {
                        if (clipField.text.length > 0) {
                            //: go button
                            //% "Go"
                            qsTrId("youtube-dl-clip-go-button")
                        } else {
                            //: paste button
                            //% "Paste"
                            qsTrId("youtube-dl-clip-paste-button")
                        }
                    }
                    onClicked: {
                        if (clipField.text.length === 0) {
                            clipField.forceActiveFocus()
                            clipField.selectAll()
                            clipField.paste()
                            clipField.softwareInputPanelEnabled = true
                        } else {
                            clipField.text = clipField.text.replace("https",
                                                                    "ytdls")
                            clipField.text = clipField.text.replace("http",
                                                                    "ytdl")
                            console.info("opening URL: " + clipField.text)
                            conf.sync()
                            Qt.openUrlExternally(clipField.text)
                            clipField.text = ""
                            app.deactivate()
                        }
                    }
                }
            } //ButtonLayout
            SilicaItem { id: spacer
                height: Theme.itemSizeMedium
                width: parent.width - Theme.paddingLarge

                Separator {
                    color: Theme.primaryColor
                    horizontalAlignment: Qt.AlignHCenter
                }
            }
            TextSwitch { id: audioOption
                //: settings enable audio extraction
                //% "Only keep audio"
                text: qsTrId("youtube-dl-audio-only-enable")
                //: settings enable audio description
                //% "Download and extract only the audio track. It will be stored under"
                description: qsTrId("youtube-dl-audio-only-enable-desc")
                             + StandardPaths.music + "/" + conf.storagePath + "."

                checked: conf.audioOnly
                onClicked: {
                    conf.audioOnly = !conf.audioOnly
                }
            }

            Slider {
                visible: audioOption.checked
                width: parent.width - Theme.paddingLarge
                //:  audio quality slider
                //% "Audio Quality"
                label: qsTrId("youtube-dl-audio-quality")
                minimumValue: 0; maximumValue: 9; stepSize: 1
                handleVisible: down
                value: 5
                valueText: {
                  if ( value == 9 ) return "♫♫♫"
                  if ( value < 9 && value >= 6 ) return "♫♫"
                  if ( value < 6 && value >= 4 ) return "♪♫"
                  if ( value < 4 && value >= 1 ) return "♪♪"
                  if ( value == 0 ) return "♪"
                }
                onReleased: {
                    conf.audioQuality = 9 - value
                }
            }
        } //Column
        //VerticalScrollDecorator { flickable: column }
    } //Flickable
} // end of Page

// vim: ft=javascript expandtab ts=4 sw=4 st=4
