import QtQuick 2.0
import Sailfish.Silica 1.0
import Nemo.Configuration 1.0
import Nemo.DBus 2.0

Page {
    id: page
    allowedOrientations: Orientation.All

    ConfigurationGroup {
        id: conf
        path: "/org/nephros/youtube-dl/mime-helper"
        property bool sponsorBlock: false
        property bool openFile: false
        property bool preferFree: true
        property bool useNetrc: false
        property int audioQuality: 5
        property int videoQuality: 0
        property string storagePath: "youtube-dl"
    }

    // generic/default properties
    property string storagePathDefault: "youtube-dl"

    DBusInterface {
        id: systemd

        bus: DBus.SessionBus
        service: "org.freedesktop.systemd1"
        path: "/org/freedesktop/systemd1"
        iface: "org.freedesktop.systemd1.Manager"
        signalsEnabled: true

        function runService() {
            systemd.typedCall("StartUnit", [{
                                                "type": "s",
                                                "value": "youtube-dl-open-installer.service"
                                            }, {
                                                "type": "s",
                                                "value": "fail"
                                            }], function (result) {
                                                console.debug('Systemd call result: ' + result)
                                            }, function (error, message) {
                                                console.warn('Systemd call failed: ',
                                                            error, 'message: ',
                                                            message)
                                            })
        }
    }

    SilicaFlickable {
        id: flick
        anchors.fill: parent
        contentHeight: column.height

        RemorsePopup {
            id: remorse
            //: reset remorse popup text
            //% "Clearing config settings"
            text: qsTrId("settings-youtube-dl-reset-remorse")
        }

        PullDownMenu {
            MenuItem {
                //: reset button
                //% "Reset all to Default"
                text: qsTrId("settings-youtube-dl-reset")
                onClicked: {
                    //console.debug("Clicked option Reset")
                    remorse.execute(qsTrId(
                                        "settings-youtube-dl-reset-remorse"),
                                    function () {
                                        conf.clear()
                                    })
                }
            }
        }

        Column {
            id: column
            width: parent.width

            SectionHeader {
                //% "Options"
                text: qsTrId("options")
            }
            TextSwitch {
                width: parent.width
                id: pathSwitch
                //: settings enable path input
                //% "Specify storage location"
                text: qsTrId("settings-youtube-dl-path-enable")
                //: settings enable path description
                //% "This should be a simple name without special characters to avoid breakage. It will be created under "
                description: qsTrId(
                                 "settings-youtube-dl-path-enable-desc") + StandardPaths.videos

                //checked: pathField.text.length > 0
                checked: conf.storagePath != storagePathDefault
                onClicked: {
                    //pathField.enabled = checked
                    checked ? true : conf.storagePath = storagePathDefault
                }
            }
            TextField {
                width: parent.width
                id: pathField
                enabled: pathSwitch.checked
                inputMethodHints: Qt.ImhNoPredictiveText | Qt.ImhNoAutoUppercase
                horizontalAlignment: TextInput.AlignHCenter
                //: settings menu switch label
                //% "A directory under: "
                label: qsTrId("settings-youtube-dl-path-label") + "~/Videos"
                placeholderText: text.length > 0 ? conf.storagePath : storagePathDefault
                text: conf.storagePath
                // do a little calidation on names, we want this to be a simple word, no path, no variables etc.
                //validator: RegExpValidator { regExp: /^[a-zA-Z0-9\-\_\/.]+$/ } // allow characters
                validator: RegExpValidator {
                    regExp: /^[-\w\.]+$/
                } // allow characters
                //validator: RegExpValidator { regExp: /^[^\0 !$~`&*()+\\\/]+$/ }    // disallow characters
                onClicked: {
                    selectAll()
                    cut()
                }
                EnterKey.enabled: text.length > 0
                //EnterKey.iconSource: "image://theme/icon-m-enter"
                EnterKey.onClicked: {
                    conf.storagePath = text || storagePathDefault
                    focus = false
                }
            }
            TextSwitch {
                width: parent.width
                id: openSwitch
                //: settings page open file
                //% "Open file after download"
                text: qsTrId("settings-youtube-dl-open")
                //: settings page open file description
                //% "This is a security risk"
                description: qsTrId("settings-youtube-dl-open-description")

                checked: conf.openFile
                onClicked: {
                    conf.openFile = checked
                }
            }
            TextSwitch {
                width: parent.width
                id: netrcSwitch
                //% "Respect ~/.netrc if it exists"
                text: qsTrId("settings-youtube-dl-prefer-use-netrc")
                //: netrc description
                //% "You can create a file called ~/.netrc and store user and password information there. This can be used to enable services which require a login to access. See the About page for more information."
                description: qsTrId(
                                 "settings-youtube-dl-prefer-use-netrc-description")

                checked: conf.useNetrc
                onClicked: {
                    conf.useNetrc = checked
                }
            }
            TextSwitch { id: blockOption
                //: settings enable SponsorBlock
                //% "Remove annoying segments from Videos."
                text: qsTrId("settings-youtube-dl-sponsorblock-enable")
                //: settings enable SponsorBlock description
                //% "Use SponsorBlock integration to remove ads etc."
                description: qsTrId("settings-youtube-dl-sponsorblock-enable-desc")
                checked: conf.sponsorBlock
                onClicked: {
                    conf.sponsorBlock = !conf.sponsorBlock
                }
            }
            TextSwitch {
                width: parent.width
                id: formatSwitch
                //% "Prefer Free formats"
                text: qsTrId("prefer-free-formats")

                checked: conf.preferFree
                onClicked: {
                    conf.preferFree = checked
                }
            }
            ComboBox {
                id: vqBox
                width: parent.width
                //% "Video Quality"
                label: qsTrId("video-quality")
                currentIndex: conf.videoQuality

                menu: ContextMenu {
                    MenuItem {
                        //% "automatic \(best\)"
                        text: qsTrId("vq-automatic")
                    } // default
                    MenuItem {
                        //% "max 1080p"
                        text: qsTrId("vq-1080")
                    }
                    MenuItem {
                        //% "max 720p"
                        text: qsTrId("vq-720")
                    }
                    MenuItem {
                        //% "max 480p"
                        text: qsTrId("vq-480")
                    }
                    MenuItem {
                        //% "worst"
                        text: qsTrId("vq-worst")
                    }
                    MenuItem {
                        //% "all available (use with caution!)"
                        text: qsTrId("vq-all")
                    }
                }
                onCurrentItemChanged: {
                    if (currentItem) {
                        conf.videoQuality = currentIndex
                    }
                }
            }
        } //Column
        VerticalScrollDecorator {
            flickable: column
        }
    } //Flickable
} //Page

// vim: ft=javascript expandtab ts=4 sw=4 st=4

