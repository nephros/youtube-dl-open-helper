import QtQuick 2.0
import Sailfish.Silica 1.0

Page { id: aboutPage
    canNavigateForward: false
    allowedOrientations: Orientation.All

    SilicaFlickable { id: flick
        anchors.fill: parent
        contentHeight: aboutColumn.height

        Column { id: aboutColumn
            width: parent.width
            spacing: Theme.paddingLarge

            PageHeader { id: aboutHeader
                title: qsTrId("youtube-dl-clip-aboutlink") + " " + qsTrId(
                           "youtube-dl-clip-cover")
            }
            Label {
                anchors { left: parent.left; right: parent.right; margins: Theme.horizontalPageMargin }
                width: parent.width - 2 * x
                //: about text
                //% "This is a convoluted collection of glue to trigger the excellent youtube-dl program. See below for links."
                text: qsTrId("youtube-dl-clip-about")
                //horizontalAlignment: Text.AlignJustify
                wrapMode: Text.Wrap
            }
            SectionHeader {
                //% "Credits/Thanks/Links"
                text: qsTrId("youtube-dl-clip-about-thanks")
            }
            Separator {
                width: parent.width - Theme.horizontalPageMargin * 2
                color: Theme.secondaryHighlightColor
                horizontalAlignment: Qt.AlignHCenter
            }
            LinkedLabel {
                anchors { left: parent.left; right: parent.right; margins: Theme.horizontalPageMargin }
                width: parent.width - 2 * x
                plainText: "Application Repo:\nhttps://openrepos.net/user/14761/programs"
                horizontalAlignment: Label.AlignHCenter
                wrapMode: Text.Wrap
                font.pixelSize: Theme.fontSizeSmall
            }
            LinkedLabel {
                anchors { left: parent.left; right: parent.right; margins: Theme.horizontalPageMargin }
                width: parent.width - 2 * x
                plainText: "Source Repo:\nhttps://codeberg.org/nephros/youtube-dl-open-helper"
                horizontalAlignment: Label.AlignHCenter
                wrapMode: Text.Wrap
                font.pixelSize: Theme.fontSizeSmall
            }
            LinkedLabel {
                anchors { left: parent.left; right: parent.right; margins: Theme.horizontalPageMargin }
                width: parent.width - 2 * x
                plainText: "Youtube-DL:\nhttps://en.wikipedia.org/wiki/Youtube-DL"
                horizontalAlignment: Label.AlignHCenter
                wrapMode: Text.Wrap
                font.pixelSize: Theme.fontSizeSmall
            }
            LinkedLabel {
                anchors { left: parent.left; right: parent.right; margins: Theme.horizontalPageMargin }
                width: parent.width - 2 * x
                plainText: "How to use ~/.netrc:\nhttps://linux.die.net/man/5/netrc"
                horizontalAlignment: Label.AlignHCenter
                wrapMode: Text.Wrap
                font.pixelSize: Theme.fontSizeSmall
            }
            SectionHeader {
                //% "Translations"
                text: qsTrId("youtube-dl-about-translations")
            }
            LinkedLabel {
                anchors { left: parent.left; right: parent.right; margins: Theme.horizontalPageMargin }
                width: parent.width - 2 * x
                wrapMode: Text.Wrap
                horizontalAlignment: Label.AlignHCenter
                plainText: "语言 (zh_CN):\ndashinfantry\nhttps://gitlab.com/dashinfantry"
                font.pixelSize: Theme.fontSizeSmall
            }
            LinkedLabel {
                anchors { left: parent.left; right: parent.right; margins: Theme.horizontalPageMargin }
                width: parent.width - 2 * x
                wrapMode: Text.Wrap
                horizontalAlignment: Label.AlignHCenter
                plainText: "Deutsch (de):\nYours Truly\nhttps://codeberg.org/nephros"
                font.pixelSize: Theme.fontSizeSmall
            }
            Item {
                height: Theme.paddingLarge * 2
                width: 1
            }
            LinkedLabel {
                anchors { left: parent.left; right: parent.right; margins: Theme.horizontalPageMargin }
                width: parent.width - 2 * x
                //anchors.bottom: aboutColumn.bottom
                color: Theme.secondaryHighlightColor
                horizontalAlignment: Label.AlignHCenter
                font.pixelSize: Theme.fontSizeSmall
                wrapMode: Text.Wrap
                plainText: "© 2020-2021 nephros <sailfish@nephros.org>\n http://public.nephros.org/sailfish \n Released under the X11 license \n https://directory.fsf.org/wiki/License:X11"
            }
        } //Column
    }
    VerticalScrollDecorator { flickable: flick }
}//Page

// vim: ft=javascript expandtab ts=4 sw=4 st=4
