<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name></name>
    <message id="settings-youtube-dl-path-enable">
        <source>Specify storage location</source>
        <oldsource>Specify download location</oldsource>
        <extracomment>settings enable path input</extracomment>
        <translation>指定下载位置</translation>
    </message>
    <message id="settings-youtube-dl-path-enable-desc">
        <source>This should be a simple name without special characters to avoid breakage. It will be created under </source>
        <extracomment>settings enable path description</extracomment>
        <translation>此处应该是一个不含特殊字符的简单名称，以避免出现问题。它将被创建在</translation>
    </message>
    <message id="settings-youtube-dl-path-desc">
        <source>Path to store videos under</source>
        <extracomment>settings menu switch description</extracomment>
        <translation type="vanished">保存文件的路径</translation>
    </message>
    <message id="settings-youtube-dl-path-label">
        <source>A directory under: </source>
        <extracomment>settings menu switch label</extracomment>
        <translation>目录位于:</translation>
    </message>
    <message id="settings-youtube-dl-open">
        <source>Open file after download</source>
        <extracomment>settings page open file</extracomment>
        <translation>下载后打开文件</translation>
    </message>
    <message id="settings-youtube-dl-open-description">
        <source>This is a security risk</source>
        <extracomment>settings page open file description</extracomment>
        <translation>可能会有安全风险</translation>
    </message>
    <message id="video-quality">
        <source>Video Quality</source>
        <translation>视频质量</translation>
    </message>
    <message id="vq-automatic">
        <source>automatic \(best\)</source>
        <oldsource>automatic (best)</oldsource>
        <translation>自动(最佳)</translation>
    </message>
    <message id="audio-quality-vbr">
        <source>Audio Quality (VBR)</source>
        <translation type="vanished">音频质量(VBR)</translation>
    </message>
    <message id="prefer-free-formats">
        <source>Prefer Free formats</source>
        <translation>偏好自由格式</translation>
    </message>
    <message id="options">
        <source>Options</source>
        <translation>操作</translation>
    </message>
    <message id="vq-1080">
        <source>max 1080p</source>
        <translation>最大1080p</translation>
    </message>
    <message id="vq-720">
        <source>max 720p</source>
        <translation>最大720p</translation>
    </message>
    <message id="vq-480">
        <source>max 480p</source>
        <translation>最大480p</translation>
    </message>
    <message id="vq-worst">
        <source>worst</source>
        <translation>最糟</translation>
    </message>
    <message id="vq-all">
        <source>all available (use with caution!)</source>
        <translation>全部可用(请谨慎使用!)</translation>
    </message>
    <message id="settings-youtube-dl-prefer-use-netrc">
        <source>Respect ~/.netrc if it exists</source>
        <translation>遵守 ~/.netrc ，如果存在</translation>
    </message>
    <message id="settings-youtube-dl-prefer-use-netrc-description">
        <source>You can create a file called ~/.netrc and store user and password information there. This can be used to enable services which require a login to access. See the About page for more information.</source>
        <extracomment>netrc description</extracomment>
        <translation>你可以创建名为 ~/.netrc 的文件并在此储存用户及密码信息。此项功能可以用于需要登录才能访问的服务。查看关于页面以获取更多信息。</translation>
    </message>
    <message id="settings-youtube-dl-reset-remorse">
        <source>Clearing config settings</source>
        <oldsource>Clearing all config values</oldsource>
        <extracomment>reset remorse popup text</extracomment>
        <translation>清空所有配置值</translation>
    </message>
    <message id="settings-youtube-dl-reset">
        <source>Reset all to Default</source>
        <extracomment>reset button</extracomment>
        <translation>重置所有</translation>
    </message>
    <message id="youtube-dl-clip-cover">
        <source>Video\nDownloader</source>
        <extracomment>name displayed on cover</extracomment>
        <translation>视频\n下载器</translation>
    </message>
    <message id="youtube-dl-clip-aboutlink">
        <source>About</source>
        <extracomment>about button</extracomment>
        <translation>关于</translation>
    </message>
    <message id="youtube-dl-clip-about">
        <source>This is a convoluted collection of glue to trigger the excellent youtube-dl program. See below for links.</source>
        <extracomment>about text</extracomment>
        <translation>这是一个复杂的 glue 数据集用以打开优质的 youtube-dl 程序。请查看下方链接。</translation>
    </message>
    <message id="youtube-dl-clip-about-thanks">
        <source>Credits/Thanks/Links</source>
        <translation>信誉/致谢</translation>
    </message>
    <message id="youtube-dl-about-translations">
        <source>Translations</source>
        <translation>翻译者</translation>
    </message>
    <message id="youtube-dl-clip-settingslink">
        <source>Settings</source>
        <extracomment>settings button</extracomment>
        <translation>设置</translation>
    </message>
    <message id="youtube-dl-clip-header">
        <source>Video URL download helper</source>
        <extracomment>clipboard page title/header</extracomment>
        <translation>视频链接下载助手</translation>
    </message>
    <message id="youtube-dl-clip-explain">
        <source>If what you have in your clipboard is a video URL, paste it here. You can either download right away, or modify the clipboard contents first, and then download.</source>
        <extracomment>short explanation below buttons</extracomment>
        <translation>如果剪贴板里的是视频网址，请粘贴在此。你可以马上下载，也可以先修改剪贴板内容，然后再下载。</translation>
    </message>
    <message id="youtube-dl-clip-paste-label">
        <source>A video URL</source>
        <extracomment>paste area</extracomment>
        <translation>视频链接</translation>
    </message>
    <message id="youtube-dl-clip-paste-button">
        <source>Paste</source>
        <extracomment>paste button</extracomment>
        <translation>粘贴</translation>
    </message>
    <message id="youtube-dl-clip-go-button">
        <source>Go</source>
        <extracomment>go button</extracomment>
        <translation>前往</translation>
    </message>
    <message id="youtube-dl-audio-only-enable">
        <source>Only keep audio</source>
        <extracomment>settings enable audio extraction</extracomment>
        <translation>仅保存音频</translation>
    </message>
    <message id="youtube-dl-audio-only-enable-desc">
        <source>Download and extract only the audio track. It will be stored under</source>
        <oldsource>Download and extract only the audio track. It will be stored under ~/Music</oldsource>
        <extracomment>settings enable audio description</extracomment>
        <translation>下载并抽取音频。文件会保存于</translation>
    </message>
    <message id="youtube-dl-audio-quality">
        <source>Audio Quality</source>
        <extracomment>audio quality slider</extracomment>
        <translation>音频质量</translation>
    </message>
    <message id="settings-youtube-dl-header">
        <source>Video URL download helper</source>
        <extracomment>settings page title/header</extracomment>
        <translation type="obsolete">视频链接下载助手</translation>
    </message>
    <message id="settings-youtube-dl-sponsorblock-enable">
        <source>Remove annoying segments from Videos.</source>
        <extracomment>settings enable SponsorBlock</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="settings-youtube-dl-sponsorblock-enable-desc">
        <source>Use SponsorBlock integration to remove ads etc.</source>
        <extracomment>settings enable SponsorBlock description</extracomment>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
