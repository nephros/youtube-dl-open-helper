#!/bin/sh

dtd="${HOME}"/.local/share/applications
dtf="${dtd}"/youtube-dl-open-url.desktop
tpl=/usr/share/youtube-dl-open-helper/youtube-dl-open-url.desktop
if [ -e "${HOME}" ]; then
	if [ -e "$dtf" ]; then
	  rm "$dtf"
	  dconf write /org/nephros/youtube-dl/mime-helper/enabled false
	  echo "$0: youtube-dl URL Helper removed from ${dtf}"
	else
	  cp "$tpl" "$dtf"
	  dconf write /org/nephros/youtube-dl/mime-helper/enabled true
	  echo "$0: URL Helper installed at ${dtf}"
	fi
fi
update-desktop-database "$dtd"
