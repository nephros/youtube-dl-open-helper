#!/usr/bin/env bash

# in case we use your very own URI scheme, make a http url of it again
url="$1"
case $url in
  ytdl*)
	url=${url/ytdl/http}
	echo transmogrifying url scheme
	;;
  ytdls*)
	url=${url/ytdls/https}
	echo transmogrifying url scheme
	;;
esac

# read settings from here:
dcpath="/org/nephros/youtube-dl/mime-helper"

# declare array
ytdl_opts=()
# detect interactive shell:
if tty -s ; then
  echo running in interactive shell.
  ytdl_opts+=( '--verbose ')
else
  ytdl_opts+=( '--quiet ')
fi
ytdl_opts+=( '--no-config-locations' )
ytdl_opts+=( '--no-playlist' )
ytdl_opts+=( '--retries 5' )
ytdl_opts+=( '--restrict-filenames' )
ytdl_opts+=( '--no-overwrites' )
# this breaks thumbnail embedding, as only mp4 type is supported
#ytdl_opts+=( '--merge-output-format mkv' )
#ytdl_opts+=( '--merge-output-format mp4' )
ytdl_opts+=( '--remux-video "aac>m4a/mov>mp4/mkv"')
ytdl_opts+=( '--paths tmp:~/.cache/yt-dlp/tmp' )
ytdl_opts+=( '--paths thumbnail:~/.cache/yt-dlp/thumbs' )
ytdl_opts+=( '--cache-dir ~/.cache/yt-dlp' )

outfile_pattern='%(title)s_%(id)s.%(ext)s'
outdir_prefix=~/Videos

## audio only
if [[ x"$(dconf read ${dcpath}/audioOnly)" = x"true" ]]; then
  outdir_prefix=~/Music
  ytdl_opts+=( '--extract-audio' )
  ytdl_opts+=( '--add-metadata' )
else
  ytdl_opts+=( '--embed-thumbnail' )
fi
## sponsorblock
if [[ x"$(dconf read ${dcpath}/sponsorBlock)" = x"true" ]]; then
  ytdl_opts+=( '--sponsorblock-remove sponsor,selfpromo,preview,filler,music_offtopic' )
fi

outdir="${outdir_prefix}"/youtube-dl
# dconf returns strings enclosed in single quotes. xargs echo removes them.
outdir_conf=$(dconf read ${dcpath}/storagePath | xargs echo)
outdir_default=$(dconf read ${dcpath}/storagePathDefault | xargs echo)
if [[ -z "${outdir_conf}" ]]; then
	if [[ -z "${outdir_default}" ]]; then
		outdir="${outdir_prefix}"/youtube-dl
	else
		outdir="${outdir_prefix}"/"${outdir_default}"
	fi
else
	outdir="${outdir_prefix}"/"${outdir_conf}"
fi

ytdl_opts+=( "--output ${outdir}/${outfile_pattern}" )

## prefer free switch:
if [[ x"$(dconf read ${dcpath}/preferFree)" = x"true" ]]; then
  ytdl_opts+=( '--prefer-free-formats' )
fi

# respect netrc setting
if [[ x"$(dconf read ${dcpath}/useNetrc)" = x"true" ]]; then
  ytdl_opts+=( '--netrc' )
fi

##  set video quality
case $(dconf read ${dcpath}/videoQuality) in
	1) # 1080
	  ytdl_opts+=( '--format "bestvideo[height<=1080]+bestaudio/best[height<=1080]"' )
	  ;;
	2) # 720
	  ytdl_opts+=( '--format "bestvideo[height<=720]+bestaudio/best[height<=720]"' )
	  ;;
	3) # 480
	  ytdl_opts+=( '--format "bestvideo[height<=480]+bestaudio/best[height<=480]"' )
	  ;;
	4) # worst
	  ytdl_opts+=( '--format worst' )
	  ;;
	5) # all
	  ytdl_opts+=( '--all-formats' )
	  ;;
	*)
	  ;;
esac


## set audio quality
aq=$(dconf read ${dcpath}/audioQuality)
if [[ ! -z "${aq}" ]] && [[ ${aq} -ge 0 ]] && [[ ${aq} -le 9 ]]; then
  ytdl_opts+=( "--audio-quality ${aq}" )
fi

### option parsing finished, lets dump the array to a temporary config file:
tmppath=$(mktemp -d -p ${XDG_RUNTIME_DIR:=/tmp})
cfgfile=${tmppath}/ytdl.conf
echo option parsing finished, will generate:
for o in "${ytdl_opts[@]}"; do echo "$o"; done
for o in "${ytdl_opts[@]}"; do echo "$o"; done > "$cfgfile"


openfile=true
openfile_conf=$(dconf read /org/nephros/youtube-dl/mime-helper/openFile)
if [[ -z "${openfile_conf}" ]]; then
	openfile=false
else
	openfile=$openfile_conf
fi

msg_start="Video download started in the background."
msg_done="Video download finished, opening..."
msg_fail="Video download failed :("

### multilanguage support:
# find out where we live:
srcdir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
if [[ -r "${srcdir}"/youtube-dl-open-helper.i18n ]]; then
  . "${srcdir}"/youtube-dl-open-helper.i18n
fi

[[ -e "$outdir" ]] || mkdir -p "$outdir"
notificationtool -o add --icon=icon-lock-transfer --application="Video-DL" --urgency=2 -T 2000 "" "" "$msg_start"

# register progress:
dconf write ${dcpath}/isActive 1

## try the download first:
errmsg=$(yt-dlp --no-warnings --simulate --config-location $cfgfile --exec 'notificationtool -o add --icon=icon-s-accept --application="Video-DL" --urgency=2 -T 2000 "" "" "Video download finished, opening..." ; xdg-open {}' "$url" 2>&1 > /dev/null )
ret=$?
errmsg=${errmsg%%;*} # only keep message before first semikolon
if [[ $ret -ne 0 ]]; then
	notificationtool -o add --icon=icon-lock-warning --application="Video-DL" --urgency=2 -T 2000 "" "" "$msg_fail" "${errmsg}"
else
  if [[ $openfile = true ]]; then
	yt-dlp --config-location $cfgfile --exec "notificationtool -o add --icon=icon-s-accept --application=\"Video-DL\" --urgency=2 -T 2000 \"\" \"\" \"$msg_done\" ; xdg-open {}" "$url" 
  else
	yt-dlp --config-location $cfgfile --exec "notificationtool -o add --icon=icon-s-accept --application=\"Video-DL\" --urgency=2 -T 2000 \"\" \"\" \"$msg_done\"" "$url" 
  fi
fi


## clean up
dconf reset ${dcpath}/audioOnly
dconf reset ${dcpath}/audioQuality
dconf write ${dcpath}/isActive 0
rm "${cfgfile}"
rmdir "${tmppath}"
