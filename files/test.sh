#!/usr/bin/env bash
#testurl=https://www.youtube.com/watch?v=zsVgoO65ykQ
testurl='https://www.youtube.com/watch?v=tRZxM9rNyZ4'
tmp=$(mktemp -d -t ytdltest_XXXXXX)
echo '
-f "worst"
--restrict-filenames
--embed-thumbnail
--no-mtime
--audio-quality 6
' > $tmp/test-config
echo "-o $tmp/%(title)s_%(id)s.%(ext)s
" >> $tmp/test-config

yt-dlp --config-location $tmp/test-config $testurl
echo done: $?
echo =================================
file $tmp/*
yt-dlp --version
echo =================================
rm -r $tmp
echo cleaning up
echo all done.
