#!/usr/bin/env bash
#
# looks up supported video sites in the source and contstructs mime endtries for the .desktop file.
#
ytroot=$1
in=$2
types=$(grep 'URL' "$ytroot"/yt_dlp/extractor/*py | grep -E -h -o 'http[s]*://[[:alnum:]]*[^/]+' | sed 's/\\//g;' | sed "s/'$//" | sed 's/(.*\?//g' | sed 's#http[s]*:/#x-url-handler#' | sed 's/$/;/' | sort -u | paste -sd ' ' | sed 's/ //g')
sed -e "s#\(^MimeType=.*\)#\1;${types}#" "$in"
